#include <rip/components/template_component.hpp>

namespace rip::components
{

    TemplateComponent::TemplateComponent(const std::string& name, const nlohmann::json& config, 
            std::shared_ptr<emb::host::EmbMessenger> emb, 
            CompTable comps)
    : RobotComponent(name, config, emb, comps)
    {}

    std::vector<std::string> TemplateComponent::diagnosticCommands() const
    {}

    void TemplateComponent::stop()
    {}

}
